package com.artsman.healthnote.core

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

interface FlowViewModel<S, E> {
    fun getState(): Flow<S>
    fun getEvents(): Flow<E?>
    fun onConsumed(event: E)
}