package com.artsman.healthnote.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

abstract class BaseViewModel<S, E> : ViewModel(), FlowViewModel<S, E> {

    abstract fun initialState(): S

    protected val _stateHolder= MutableStateFlow(initialState())
    protected val _eventHolder = MutableSharedFlow<E?>()

    override fun getState(): Flow<S> {
        return _stateHolder
    }

    override fun getEvents(): Flow<E?> {
        return _eventHolder
    }

    fun send(state: S){
        viewModelScope.launch {
            _stateHolder.emit(state)
        }
    }
    fun fire(event: E){
        viewModelScope.launch {
            _eventHolder.emit(event)
        }
    }

    override fun onConsumed(event: E) {
        viewModelScope.launch {
            _eventHolder.emit(null)
        }
    }

}