package com.artsman.healthnote.core

interface IAction<A> {
    fun add(action: A)
}