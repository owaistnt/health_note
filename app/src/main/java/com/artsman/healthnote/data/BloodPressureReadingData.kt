package com.artsman.healthnote.data

import kotlinx.datetime.LocalDateTime

data class BloodPressureReadingData(val diastole: String, val systole: String, val date: LocalDateTime)