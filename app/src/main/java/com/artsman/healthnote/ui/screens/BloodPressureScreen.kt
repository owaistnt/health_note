package com.artsman.healthnote.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.artsman.healthnote.data.BloodPressureReadingData
import com.artsman.healthnote.features.bloodpressure.BloodPressureViewModel
import com.artsman.healthnote.features.bloodpressure.FakeBloodPressureReadingRepo
import com.artsman.healthnote.features.bloodpressure.UIState
import com.artsman.healthnote.ui.components.BloodPressureReading
import com.artsman.healthnote.ui.components.BloodPressureReadingList
import com.artsman.healthnote.ui.components.LoadingView
import kotlinx.coroutines.flow.collect
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime

@Composable
fun BloodPressureScreen(viewModel: BloodPressureViewModel) {
    val state: UIState by viewModel.getState().collectAsState(initial = UIState.Loading)
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Health Note") }
            ) },
        floatingActionButton = {
            FloatingActionButton(onClick = { /*TODO*/ }) {
                Text(text = "Add")
            }
        }) {
        Box(modifier = Modifier.padding(horizontal = 4.dp, vertical = 8.dp)){
            when (state) {
                UIState.Loading -> LoadingView()
                is UIState.Update -> BloodPressureReadingList(readings = (state as UIState.Update).viewData.readings)
            }
        }


    }
}


@Preview
@Composable
fun BloodPressureScreenPreview() {
    BloodPressureScreen(
        viewModel = BloodPressureViewModel(bloodPressureRepo = FakeBloodPressureReadingRepo())
    )
}