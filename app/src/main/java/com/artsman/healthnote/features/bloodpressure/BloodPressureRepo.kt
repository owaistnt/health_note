package com.artsman.healthnote.features.bloodpressure

import com.artsman.healthnote.data.BloodPressureReadingData
import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.LocalDateTime

interface IBloodPressureRepo {
    suspend fun getReading(): List<BloodPressureReadingData>
}



class FakeBloodPressureReadingRepo: IBloodPressureRepo{
    override suspend fun getReading(): List<BloodPressureReadingData> {
        return listOf(
            BloodPressureReadingData(
                "88",
                "124",
                LocalDateTime(2000, 6, 20, 0, 0, 0)
            ), BloodPressureReadingData(
                "88",
                "124",
                LocalDateTime(2000, 6, 19, 0, 0, 0)
            )
        )
    }

}