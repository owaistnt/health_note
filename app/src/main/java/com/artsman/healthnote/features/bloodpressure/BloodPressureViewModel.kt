package com.artsman.healthnote.features.bloodpressure

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.artsman.healthnote.core.BaseViewModel
import com.artsman.healthnote.core.IAction
import com.artsman.healthnote.data.BloodPressureReadingData
import kotlinx.coroutines.launch

class BloodPressureViewModel(private val bloodPressureRepo: IBloodPressureRepo): BaseViewModel<UIState, UIEvent>(), IAction<Action> {

    override fun add(action: Action) {
        when(action){
            Action.Start -> viewModelScope.launch {
                val readings = bloodPressureRepo.getReading()
                val updatedState = UIState.Update(viewData = ViewData(readings))
                send(updatedState)
            }
        }
    }

    override fun initialState(): UIState {
        return UIState.Loading
    }

}

data class ViewData(val readings: List<BloodPressureReadingData>)

sealed class UIState{
    object Loading: UIState()
    data class Update(val viewData: ViewData): UIState()
}

sealed class UIEvent{

}

sealed class Action{
    object Start: Action()
}