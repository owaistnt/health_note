package com.artsman.healthnote.ui.components

import android.os.SystemClock
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import java.time.format.TextStyle
import java.util.*


@Composable
fun BloodPressureReading(systole: String, daistole: String, localDate: LocalDateTime) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(horizontal = 8.dp, vertical = 4.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.padding(horizontal = 8.dp, vertical = 8.dp)
        ) {
            Text(
                text = "${localDate.dayOfMonth} ${localDate.month} ${
                    localDate.year.toString().subSequence(0, 2)
                }",
                textAlign = TextAlign.End
            )
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Systole: $systole")
                Text(text = "Daistole: $daistole")
            }
        }

    }
}

@Composable
@Preview
fun BloodPressureReadingPreview() {
    BloodPressureReading("124", "75", LocalDateTime(2000, 6, 20, 0, 0, 0))
}