package com.artsman.healthnote

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.artsman.healthnote.features.bloodpressure.Action
import com.artsman.healthnote.features.bloodpressure.BloodPressureViewModel
import com.artsman.healthnote.features.bloodpressure.FakeBloodPressureReadingRepo
import com.artsman.healthnote.ui.screens.BloodPressureScreen
import com.artsman.healthnote.ui.theme.HealthnoteTheme

class MainActivity : ComponentActivity() {

    lateinit var bloodPressureViewModel: BloodPressureViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bloodPressureViewModel = BloodPressureViewModel(bloodPressureRepo = FakeBloodPressureReadingRepo())
        setContent {
            HealthnoteTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    bloodPressureViewModel.add(Action.Start)
                    BloodPressureScreen(viewModel = bloodPressureViewModel)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
    }
}