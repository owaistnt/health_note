package com.artsman.healthnote.ui.components

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.artsman.healthnote.data.BloodPressureReadingData
import kotlinx.datetime.LocalDateTime

@Composable
fun BloodPressureReadingList(readings: List<BloodPressureReadingData>) {
    LazyColumn {
        items(readings, key = { index -> index.date.toString() }) { reading ->
            BloodPressureReading(
                systole = reading.systole,
                daistole = reading.diastole,
                localDate = reading.date
            )
        }
    }
}

@Preview
@Composable
private fun BloodPressureReadingListPreview() {
    BloodPressureReadingList(
        readings = listOf(
            BloodPressureReadingData(
                "88",
                "124",
                LocalDateTime(2000, 6, 20, 0, 0, 0)
            ), BloodPressureReadingData(
                "88",
                "124",
                LocalDateTime(2000, 6, 19, 0, 0, 0)
            )
        )
    )
}
